import javax.jms.*;

public class JMSListener implements MessageListener {

    public void onMessage(Message message){

        if (message instanceof TextMessage) {
            String msg = null;
            String correlationID = null;
            try {
                msg = message.getBody(String.class);
                correlationID = message.getJMSCorrelationID();
            } catch (JMSException e) {
                e.printStackTrace();
            }
            String finalMsg = msg;
            String finalCorrID = correlationID;

            Thread t1 = new Thread(() -> {
               JMSProducer jmsProducer = new JMSProducer(finalMsg, finalCorrID);
               jmsProducer.run();
            });
            t1.start();
        }
    }
}
