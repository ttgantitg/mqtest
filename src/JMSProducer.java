import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.jms.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

class JMSProducer {

    private String msg;
    private String correlationID;

    JMSProducer(String msg, String correlationID) {
        this.correlationID = correlationID;
        this.msg = msg;
    }

    void run() {

        ByteArrayInputStream input = null;
        JMSContext context = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            input = new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
            Document doc = Objects.requireNonNull(builder).parse(input);
            NodeList nodeList = Objects.requireNonNull(doc).getElementsByTagName("guid");
            String guid = nodeList.item(0).getTextContent();
            String xmlMessage = "<testRs>\n" +
                    "\t<status> OK </status>\n" +
                    "\t<guid>" + " " + guid + " " + "</guid>\n" +
                    "</testRs>";
            try {
                context = MQConnect.connect();
                Destination destination = context.createQueue("queue:///" + "TEST.OUT");
                TextMessage message = context.createTextMessage(xmlMessage);
                message.setJMSCorrelationID(correlationID);
                javax.jms.JMSProducer producer = context.createProducer();
                producer.send(destination, message);
            } catch (JMSException e) {
                e.printStackTrace();
            } finally {
                assert context != null;
                context.close();
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert input != null;
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
