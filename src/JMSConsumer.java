import javax.jms.*;

public class JMSConsumer {

    public static void main(String[] args) {

        JMSContext context = MQConnect.connect();
        Destination destination = context.createQueue("queue:///" + "TEST.IN");
        javax.jms.JMSConsumer consumer = context.createConsumer(destination);
        JMSListener JMSListener = new JMSListener();
        consumer.setMessageListener(JMSListener);

        while (true){}
    }
}
